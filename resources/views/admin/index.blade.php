@extends('layouts.master')
@section('content')
    <!--Begin::Main Portlet-->
    <div class="row">
        <div class="col-xl-12">
            <!--begin:: Widgets/Activity-->
            <div class="m-portlet m-portlet--bordered-semi m-portlet--widget-fit m-portlet--full-height m-portlet--skin-light ">
                
                <div class="m-portlet__body">
                    <div class="m-widget17">
                        <div class="m-widget17__visual m-widget17__visual--chart m-portlet-fit--top m-portlet-fit--sides m--bg-danger">
                            <div class="m-widget17__chart" >
                                <!-- <canvas id="m_chart_activities"></canvas> -->
                            </div>
                        </div>
                        <div class="m-widget17__stats">
                            <div class="m-widget17__items m-widget17__items-col1">
                                <div class="m-widget17__item">
                                    <span class="m-widget17__icon">
                                        <i class="flaticon-truck m--font-brand"></i>
                                    </span>
                                    <span class="m-widget17__subtitle">
                                        Orders
                                    </span>
                                    <span class="m-widget17__desc">
                                        {{ count($orders) }}
                                    </span>
                                </div>
                                
                                <div class="m-widget17__item">
                                    <span class="m-widget17__icon">
                                        <i class="flaticon-pie-chart m--font-success"></i>
                                    </span>
                                    <span class="m-widget17__subtitle">
                                        Categories
                                    </span>
                                    <span class="m-widget17__desc">
                                        {{ count($categories) }}
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget17__items m-widget17__items-col2">
                                <div class="m-widget17__item">
                                    <span class="m-widget17__icon">
                                        <i class="flaticon-time m--font-danger"></i>
                                    </span>
                                    <span class="m-widget17__subtitle">
                                        Products
                                    </span>
                                    <span class="m-widget17__desc">
                                        {{ count($products) }}
                                    </span>
                                </div>
                                
                                <div class="m-widget17__item">
                                    <span class="m-widget17__icon">
                                        <i class="flaticon-users m--font-info"></i>
                                    </span>
                                    <span class="m-widget17__subtitle">
                                        Users
                                    </span>
                                    <span class="m-widget17__desc">
                                    {{\App\Models\User::count()}}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Widgets/Activity-->
        </div>
    </div>
    <!--End::Main Portlet-->
    @endsection
