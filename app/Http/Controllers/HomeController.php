<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // die('here');
        $orders = OrderDetail::groupBy('order_id')->get();
        $products = Product::where('is_active',1)->get();
        $categories = Category::with('parentCategory')->get();

        return view('admin.index', get_defined_vars());
    }
}
